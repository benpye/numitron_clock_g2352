/*
 * uart.h
 *
 *  Created on: 9 Jan 2013
 *      Author: Ben
 */

#ifndef UART_H_
#define UART_H_

extern char g_RxBuffer[];
extern unsigned int g_RxCount;

void uartInit();
void uartTx(unsigned char byte);
void uartPrint(char *string);

#endif /* UART_H_ */

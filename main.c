/*
 * main.c
 *
 *  Created on: 3 Jan 2013
 *      Author: Ben
 */

#include <msp430.h>
#include "time.h"
#include "display.h"
#include "flashstorage.h"
#include "uart.h"

typedef union
{
	unsigned long l;
	unsigned char c[4];
} UINT32;

//------------------------------------------------------------------------------
// Hardware-related definitions
//------------------------------------------------------------------------------
#define BRIGHTNESS_BUTTON BIT2
#define MODE_BUTTON BIT1

#define ALARM_OUT BIT0 // Bit 0 on port 2

#define DEGREE_SYMBOL 0x3A


//------------------------------------------------------------------------------
// Serial Interface
//------------------------------------------------------------------------------
#define CMD_SET_TIME 0x01
#define CMD_SIZE_SET_TIME 7

#define CMD_SET_BRIGHT 0x02
#define CMD_SIZE_SET_BRIGHT 4

#define CMD_GET_SERIAL 0x03
#define CMD_SIZE_GET_SERIAL 2

#define CMD_SET_SERIAL 0x04
#define CMD_SIZE_SET_SERIAL 7

#define CMD_GET_CONFIG 0x05
#define CMD_SIZE_GET_CONFIG 2

#define CMD_SET_CONFIG 0x06
#define CMD_SIZE_SET_CONFIG 4

#define CMD_SET_ALARM 0x07
#define CMD_SIZE_SET_ALARM 5

#define CMD_RET 0xFE
#define CMD_END 0xFF

//------------------------------------------------------------------------------
// Config Defines
//------------------------------------------------------------------------------

#define ENABLE_CMD_SET_TIME
#define ENABLE_CMD_SET_BRIGHT
#define ENABLE_CMD_GET_SERIAL
#define ENABLE_CMD_SET_SERIAL
#define ENABLE_CMD_GET_CONFIG
#define ENABLE_CMD_SET_CONFIG
#define ENABLE_CMD_SET_ALARM

// INFO B Storage Table
// 0x00 -> 0x03	- Serial Number
// 0x04			- Serial Number Checksum (Used to stop it being set again)
// 0x05 		- Brightness
// 0x06			- Config Toggles (See defines below)
// 0x07			- Alarm Hour
// 0x08			- Alarm Minutes

//------------------------------------------------------------------------------
// Time related variables
//------------------------------------------------------------------------------
UINT32 unixSeconds = 0;
tTime currentTime;
tTime alarmTime;
unsigned char alarmTriggered = 0;
unsigned char timeChanged = 0;

long temperature = 0; // Temperature, stored in current mode (C or F)

unsigned char powerOut = 0; // Set to 1 when primary power fails

//------------------------------------------------------------------------------
// Settings
//------------------------------------------------------------------------------
#define BRIGHTNESS_STEPS 8
unsigned int displayBrightness;
unsigned int mode = 0;
#define DISPLAY_MODES 4

UINT32 serialNumber;
unsigned char configBools;

#define CONFIG_FARENHEIT	BIT0
#define CONFIG_12HOUR		BIT1
#define CONFIG_MMDD			BIT2
#define CONFIG_ALARM		BIT3

//------------------------------------------------------------------------------
// Debouncing
//------------------------------------------------------------------------------
unsigned int buttonTick = 128;

unsigned char calculateLRC(const unsigned char *buf, unsigned int n){
	unsigned char checksum = 0;
	while(n>0)
	{
		checksum += *buf++;
		n--;
	}
	return ((char) -checksum );
}

inline void setDisplayBrightness(unsigned char val)
{
	displayBrightness = val;
	g_FlashRamTable[5] = displayBrightness;
	flashCommitData();
}

inline void setAlarm(unsigned char hours, unsigned char minutes)
{
	alarmTime.ucMin = minutes;
	alarmTime.ucHour = hours;
	g_FlashRamTable[7] = hours;
	g_FlashRamTable[8] = minutes;
	flashCommitData();
}

void drawDisplay()
{
	switch(mode)
	{
	case 1:
		if((configBools & CONFIG_MMDD) == CONFIG_MMDD)
			displayDrawNumberDecimal((currentTime.ucMon+1) * 100 + currentTime.ucMday);
		else
			displayDrawNumberDecimal(currentTime.ucMday * 100 + currentTime.ucMon+1);
		break;
	case 2:
	{
		// Do this manually for better display
		int tempUnits = (temperature % 10);
		int tempTens = (temperature - tempUnits) / 10;
		displayLatchLow();

		if((configBools & CONFIG_FARENHEIT) == CONFIG_FARENHEIT)
			displayDrawDigit(0xF);
		else
			displayDrawDigit(0xC);

		displayShiftOut(DEGREE_SYMBOL);
		displayDrawDigit(tempUnits);
		displayDrawDigit(tempTens);
		displayLatchHigh();
		ADC10CTL0 |= ENC + ADC10SC; // Sampling and conversion start
		break;
	}
	case 3:
		if((configBools & CONFIG_ALARM) == CONFIG_ALARM)
		{
			displayDrawNumberDecimal(alarmTime.ucHour * 100 + alarmTime.ucMin);
			break;
		}
		else
		{
			mode++;
			if(mode >= DISPLAY_MODES)
				mode = 0;
		}
	default: // If invalid mode, display normal time
	{
		int hour = currentTime.ucHour;
		if((configBools & CONFIG_12HOUR) == CONFIG_12HOUR)
		{
			if(hour > 12)
				hour -= 12;
		}

		displayDrawNumberDecimal(hour * 100 + currentTime.ucMin);

		break;
	}
	}
}

//------------------------------------------------------------------------------
// main()
//------------------------------------------------------------------------------
int main(void)
{
    unsigned int i;
    WDTCTL = WDTPW + WDTHOLD;									// Disable watchdog reset

    P1OUT = 0x00;												// Initialize all GPIO
    P1DIR = 0xFF;												// Set all pins to output

    P2OUT &= ~ALARM_OUT;
    P2DIR |= ALARM_OUT;
    P2DIR &= ~(BRIGHTNESS_BUTTON + MODE_BUTTON);
    P2REN |= BRIGHTNESS_BUTTON + MODE_BUTTON;
    P2OUT &= ~(BRIGHTNESS_BUTTON + MODE_BUTTON);
    P2IES |= BRIGHTNESS_BUTTON + MODE_BUTTON;
    P2IFG &= ~(BRIGHTNESS_BUTTON + MODE_BUTTON);				// Clear button interrupts
    P2IE |= BRIGHTNESS_BUTTON + MODE_BUTTON;					// Enable button interrupts

    // Make sure we have a nice clean start
    displayDisable();
    drawDisplay();
    displayEnable();

    flashInitRamOverlay();
    for(i = 0; i < 4; i++)
    {
    	serialNumber.c[i] = g_FlashRamTable[i];
    }
    displayBrightness = g_FlashRamTable[5];
    configBools = g_FlashRamTable[6];

    alarmTime.ucMin = g_FlashRamTable[8];
    alarmTime.ucHour = g_FlashRamTable[7];

    ADC10CTL1 = INCH_10 + ADC10DIV_3; // Temp Sensor ADC10CLK/4
    ADC10CTL0 = SREF_1 + ADC10SHT_3 + REFON + ADC10ON + ADC10IE;

    // Power fail sensing
    CACTL1 = CAREF_2 + CAON + CAIE & ~(CAIFG + CAIES); // 0.5 Vcc = +comp, on
    CACTL2 = P2CA3 + P2CA2;                           // P1.6/CA6 = -comp

    // Set load capacitance for 32.768kHz crystal
    BCSCTL3 |= XCAP_2;
    // Setup watchdog timer as an interval timer
    WDTCTL = WDT_ADLY_1_9; // 512Hz 1.9ms intervals with a 32.768kHz crystal
    IE1 |= WDTIE;                                       // Enable WDT interrupt

    uartInit();                     // Start Timer_A UART

    __enable_interrupt();

    for (;;)
    {
        // Wait for incoming character
        __bis_SR_register(LPM3_bits);
        if(g_RxCount >0)
        {
			switch(g_RxBuffer[0])
			{
#ifdef ENABLE_CMD_SET_TIME
			case CMD_SET_TIME:
				if(g_RxBuffer[g_RxCount-1] == CMD_END && g_RxCount == CMD_SIZE_SET_TIME)
				{
					if(g_RxBuffer[CMD_SIZE_SET_TIME-2] == calculateLRC(g_RxBuffer+1, 4))
					{
						for(i=0; i<4; i++)
						{
							unixSeconds.c[i] = g_RxBuffer[i+1];
						}
					}
					g_RxCount=0;
				}
				else if(g_RxCount >= CMD_SIZE_SET_TIME)
					g_RxCount=0;

				break;
#endif
#ifdef ENABLE_CMD_SET_BRIGHT
			case CMD_SET_BRIGHT:
				if(g_RxBuffer[g_RxCount-1] == CMD_END && g_RxCount == CMD_SIZE_SET_BRIGHT)
				{
					if(g_RxBuffer[CMD_SIZE_SET_BRIGHT-2] == calculateLRC(g_RxBuffer+1, 1))
					{
						setDisplayBrightness(g_RxBuffer[1]);
					}
					g_RxCount=0;
				}
				else if(g_RxCount >= CMD_SIZE_SET_BRIGHT)
					g_RxCount=0;

				break;
#endif
#ifdef ENABLE_CMD_GET_SERIAL
			case CMD_GET_SERIAL:
				if(g_RxBuffer[g_RxCount-1] == CMD_END && g_RxCount == CMD_SIZE_GET_SERIAL)
				{
					uartTx(CMD_RET);
					for(i=0; i<4; i++)
					{
						uartTx(serialNumber.c[i]);
					}
					uartTx(CMD_END);
					g_RxCount=0;
				}
				else if(g_RxCount > CMD_SIZE_GET_SERIAL)
					g_RxCount=0;

				break;
#endif
#ifdef ENABLE_CMD_SET_SERIAL
			case CMD_SET_SERIAL:
				if(g_RxBuffer[g_RxCount-1] == CMD_END && g_RxCount == CMD_SIZE_SET_SERIAL)
				{
					if(g_RxBuffer[CMD_SIZE_SET_SERIAL-2] == calculateLRC(g_RxBuffer+1, 4)
							&& g_FlashRamTable[4] != calculateLRC(serialNumber.c, 4))
					{
						for(i=0; i<4; i++)
						{
							serialNumber.c[i] = g_RxBuffer[i+1];
							g_FlashRamTable[i] = serialNumber.c[i];
						}
						g_FlashRamTable[4] = g_RxBuffer[CMD_SIZE_SET_SERIAL-2];
						flashCommitData();
					}
					g_RxCount=0;
				}
				else if(g_RxCount >= CMD_SIZE_SET_SERIAL)
					g_RxCount=0;

				break;
#endif
#ifdef ENABLE_CMD_GET_CONFIG
			case CMD_GET_CONFIG:
				if(g_RxBuffer[g_RxCount-1] == CMD_END && g_RxCount == CMD_SIZE_GET_CONFIG)
				{
					uartTx(CMD_RET);
					uartTx(configBools);
					uartTx(CMD_END);
					g_RxCount=0;
				}
				else if(g_RxCount > CMD_SIZE_GET_CONFIG)
					g_RxCount=0;

				break;
#endif
#ifdef ENABLE_CMD_SET_CONFIG
			case CMD_SET_CONFIG:
				if(g_RxBuffer[g_RxCount-1] == CMD_END && g_RxCount == CMD_SIZE_SET_CONFIG)
				{
					if(g_RxBuffer[CMD_SIZE_SET_CONFIG-2] == calculateLRC(g_RxBuffer+1, 1))
					{
						configBools = g_RxBuffer[1];
						g_FlashRamTable[6] = configBools;
						flashCommitData();
					}
					g_RxCount=0;
				}
				else if(g_RxCount >= CMD_SIZE_SET_CONFIG)
					g_RxCount=0;

				break;
#endif
#ifdef ENABLE_CMD_SET_ALARM
			case CMD_SET_ALARM:
				if(g_RxBuffer[g_RxCount-1] == CMD_END && g_RxCount == CMD_SIZE_SET_ALARM)
				{
					if(g_RxBuffer[CMD_SIZE_SET_ALARM-2] == calculateLRC(g_RxBuffer+1, 2))
					{
						setAlarm(g_RxBuffer[1], g_RxBuffer[2]);
					}
					g_RxCount=0;
				}
				else if(g_RxCount >= CMD_SIZE_SET_ALARM)
					g_RxCount=0;

				break;
#endif
			default:
				g_RxCount = 0;
				break;
			}
        }

        if(timeChanged)
        {
        	timeChanged = 0;
        	ulocaltime(unixSeconds.l, &currentTime);
        	if((configBools & CONFIG_ALARM) == CONFIG_ALARM)
        	{
        		if(alarmTime.ucMin == currentTime.ucMin && alarmTime.ucHour == currentTime.ucHour && currentTime.ucSec == 0)
        		{
        			alarmTriggered = 1;
        			mode = 0; // Display the time
        		}
        	}
        	drawDisplay();
        }
    }
}

// Interrupt fired 32768/64 = 512 times a second
#pragma vector=WDT_VECTOR
__interrupt void wdt_interrupt(void)
{
	static unsigned int ticks = 0;
	static unsigned char slowWDT = 0;

	if(slowWDT)
	{
		if(!powerOut)
		{
			slowWDT = 0;
			WDTCTL = WDT_ADLY_1_9;
		}

		unixSeconds.l++;
		timeChanged = 1;
		return;
	}

	if(!powerOut)
	{
		// PWM, can't be moved unfortunately
		unsigned tempticks = ticks;
		if(tempticks > 511)
			tempticks -= 512;

		unsigned int pwmtick = tempticks%BRIGHTNESS_STEPS;

		if(alarmTriggered)
		{
			if(ticks > 256)
			{
				displayDisable();
				P2OUT &= ~ALARM_OUT;
			}
			else
			{
				displayEnable();
				P2OUT |= ALARM_OUT;
			}
		}
		else
		{
			if(pwmtick+1 > displayBrightness)
				displayDisable();
			else
				displayEnable();
		}
	}

	if(ticks > 511)
	{
		ticks = 0;
		unixSeconds.l++;
		timeChanged = 1; // We've moved foward a second

		if(powerOut)
		{
			slowWDT = 1;
			WDTCTL = WDT_ADLY_1000;
		}
		else
			__bic_SR_register_on_exit(LPM3_bits);  // Clear LPM0 bits from 0(SR)
	}

	ticks++;
	buttonTick++;
}

// Port 2 interrupt service routine
#pragma vector=PORT2_VECTOR
__interrupt void p2_interrupt(void)
{
	if(unixSeconds.l != 0) // Hack to stop this interrupt immediately firing
	{
		if(alarmTriggered)
		{
			alarmTriggered = 0; // Any button is the snooze button
			buttonTick = 0; // And lets not go changing any modes
			P2OUT &= ~ALARM_OUT; // And lets ensure that the alarm stops
		}
		else
		{
			if((P2IFG & BRIGHTNESS_BUTTON) == BRIGHTNESS_BUTTON)
			{
				if(buttonTick > 128)
				{
					displayBrightness++;
					if(displayBrightness > BRIGHTNESS_STEPS)
						displayBrightness = 0;
					setDisplayBrightness(displayBrightness);
					buttonTick = 0;
				}
			}

			if((P2IFG & MODE_BUTTON) == MODE_BUTTON)
			{
				if(buttonTick > 128)
				{
					mode++;
					if(mode >= DISPLAY_MODES)
						mode = 0;

					drawDisplay();
					buttonTick = 0;
				}
			}
		}
	}

	P2IFG &= ~P2IFG; // Clear all port 1 interrupts
}

// ADC10 interrupt service routine
#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR (void)
{
	long temp = ADC10MEM;
	if((configBools & CONFIG_FARENHEIT) == CONFIG_FARENHEIT)
		temperature = ((temp - 630) * 761) / 1024;
	else
		temperature = ((temp - 673) * 423) / 1024;
    ADC10CTL0 &= ~ENC;
}

// Power fail interrupt
#pragma vector=COMPARATORA_VECTOR
__interrupt void Comparator_ISR(void)
{
	if(!powerOut)
	{
		powerOut = 1;
		//CACTL1 &= ~(CAIE + CAON + CAIFG);
		displayOff();
		P2OUT &= ~ALARM_OUT;
		alarmTriggered = 0;
	}
	else
	{
		powerOut = 0;
	}

	CACTL1 &= ~ CAIFG;
	CACTL1 ^= CAIES;
}

/*
 * softuart.c
 *
 *  Created on: 9 Jan 2013
 *      Author: Ben
 */

#include <msp430.h>
#include "uart.h"

//------------------------------------------------------------------------------
// Global variables used for full-duplex UART communication
//------------------------------------------------------------------------------
unsigned int txData;                        // UART internal variable for TX
char g_RxBuffer[8];                         // Received UART character
unsigned int g_RxCount = 0;

#define UART_TXD   0x02                     // TXD on P1.1 (Timer0_A.OUT0)
#define UART_RXD   0x04                     // RXD on P1.2 (Timer0_A.CCI1A)

//------------------------------------------------------------------------------
// Conditions for 2400 baud, ACLK
//------------------------------------------------------------------------------
#define UART_TBIT_DIV_2     0x06
#define UART_TBIT           0x0E

//------------------------------------------------------------------------------
// Function configures Timer_A for full-duplex UART operation
//------------------------------------------------------------------------------
void uartInit(void)
{
    P1SEL |= UART_TXD + UART_RXD;			// Timer function for TXD/RXD pins
    P1DIR &= ~UART_RXD;

    TACCTL0 = OUT;                          // Set TXD Idle as Mark = '1'
    TACCTL1 = SCS + CM1 + CAP + CCIE;       // Sync, Neg Edge, Capture, Int
    TACTL = TASSEL_1 + MC_2;                // ACLK, continuous mode
}
//------------------------------------------------------------------------------
// Outputs one byte using the Timer_A UART
//------------------------------------------------------------------------------
void uartTx(unsigned char byte)
{
    while (TACCTL0 & CCIE);                 // Ensure last char got TX'd
    TACCR0 = TAR;                           // Current state of TA counter
    TACCR0 += UART_TBIT;                    // One bit time till first bit
    TACCTL0 = OUTMOD0 + CCIE;               // Set TXD on EQU0, Int
    txData = byte;                          // Load global variable
    txData |= 0x100;                        // Add mark stop bit to TXData
    txData <<= 1;                           // Add space start bit
}

//------------------------------------------------------------------------------
// Prints a string over using the Timer_A UART
//------------------------------------------------------------------------------
void uartPrint(char *string)
{
    while (*string)
    {
    	uartTx(*string++);
    }
}
//------------------------------------------------------------------------------
// Timer_A UART - Transmit Interrupt Handler
//------------------------------------------------------------------------------
#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer_A0_ISR(void)
{
    static unsigned char txBitCnt = 10;

    TACCR0 += UART_TBIT;                    // Add Offset to CCRx
    if (txBitCnt == 0)                      // All bits TXed?
    {
        TACCTL0 &= ~CCIE;                   // All bits TXed, disable interrupt
        txBitCnt = 10;                      // Re-load bit counter
    }
    else {
        if (txData & 0x01)
        {
          TACCTL0 &= ~OUTMOD2;              // TX Mark '1'
        }
        else
        {
          TACCTL0 |= OUTMOD2;               // TX Space '0'
        }
        txData >>= 1;
        txBitCnt--;
    }
}
//------------------------------------------------------------------------------
// Timer_A UART - Receive Interrupt Handler
//------------------------------------------------------------------------------
#pragma vector = TIMER0_A1_VECTOR
__interrupt void Timer_A1_ISR(void)
{
	static unsigned char rxBitCnt = 8;
    static unsigned char rxData = 0;

    switch (__even_in_range(TA0IV, TA0IV_TAIFG)) // Use calculated branching
    {
    	case TA0IV_TACCR1:                       // TACCR1 CCIFG - UART RX
    		TACCR1 += UART_TBIT;                 // Add Offset to CCRx
    		if (TACCTL1 & CAP)                   // Capture mode = start bit edge
    		{
    			TACCTL1 &= ~CAP;                 // Switch capture to compare mode
                TACCR1 += UART_TBIT_DIV_2;       // Point CCRx to middle of D0
    		}
            else {
                rxData >>= 1;
                if (TACCTL1 & SCCI)              // Get bit waiting in receive latch
                {
                    rxData |= 0x80;
                }
                rxBitCnt--;
                if (rxBitCnt == 0)               // All bits RXed?
                {
                	if(g_RxCount == 8)
                		g_RxCount = 0;           // Buffer full
                	g_RxBuffer[g_RxCount] = rxData; // Store in global variable
                	g_RxCount++;                 // Increment the buffer count
                	rxBitCnt = 8;                // Re-load bit counter
                    TACCTL1 |= CAP;              // Switch compare to capture mode
                    __bic_SR_register_on_exit(LPM3_bits);  // Clear LPM0 bits from 0(SR)
                }
            }
            break;
    }
}
//------------------------------------------------------------------------------

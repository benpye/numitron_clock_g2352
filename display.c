/*
 * display.c
 *
 *  Created on: 3 Jan 2013
 *      Author: Ben
 */

#include <msp430.h>
#include "display.h"

// Util Functions ( not in header )
// Writes a value to the specified bitmask/pin. Use built in defines
// when calling this, as the shiftOut() function does.
// All nonzero values are treated as "high" and zero is "low"
inline void displayPinWrite( unsigned int bit, unsigned char val )
{
	if (val)
	{
		P1OUT |= bit;
	}
	else
	{
		P1OUT &= ~bit;
	}
}

// Delays by the specified Milliseconds
// thanks to:
// http://www.threadabort.com/archive/2010/09/05/msp430-delay-function-like-the-arduino.aspx
void displayDelay(unsigned int ms)
{
	while (ms--)
	{
		__delay_cycles(1000); // 1000 for 1 Mhz
	}
}

// Main functions

static const short g_numsToShift[16] =
{
		0xDE, 0x90, 0xEA, 0x6E, 0x36, 0x7C, 0xFC, 0x0E, 0xFE, 0x7E, 0xBE, 0xF4, 0xD8, 0xE6, 0xF8, 0xB8
};

// Shift out digit to display, convert number to 7 segment
void displayDrawDigit( unsigned char num )
{
	displayShiftOut(g_numsToShift[num]);
}

// Display 4 or fewer digit number on the display
// Decimal format
void displayDrawNumberDecimal( unsigned int number )
{
	displayLatchLow();
	unsigned char count = 0;

	while(number > 0 && count < 4)
	{
		displayDrawDigit(number % 10);
		number /= 10;
		count++;
	}

	while(count < 4)
	{
		displayDrawDigit(0);
		count++;
	}
	displayLatchHigh();
}

// Pulse the clock pin
void displayPulseClock( void )
{
  P1OUT |= CLOCK;
  P1OUT ^= CLOCK;

}

void displayLatchLow( void )
{
	P1OUT &= ~LATCH;
}

void displayLatchHigh( void )
{
	P1OUT |= LATCH;
}

// Take the given 8-bit value and shift it out, LSB to MSB
void displayShiftOut(unsigned char val)
{
	char i;

	// Iterate over each bit, set data pin, and pulse the clock to send it
	// to the shift register
	for (i = 0; i < 8; i++)
	{
		displayPinWrite(DATA, (val & (1 << i)));
		displayPulseClock();
	}
}

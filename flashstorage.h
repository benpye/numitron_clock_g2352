/*
 * flashstorage.h
 *
 *  Created on: 4 Jan 2013
 *      Author: Ben
 */

#ifndef FLASHSTORAGE_H_
#define FLASHSTORAGE_H_

// This only needs to be as big as the data we store
// Any larger is wasted RAM
#define FLASH_RAM_BUFFER_SIZE 16

extern char g_FlashRamTable[];

void flashInitRamOverlay();
void flashCommitData();

#endif /* FLASHSTORAGE_H_ */

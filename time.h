/*
 * time.h
 *
 *  Created on: 3 Jan 2013
 *      Author: Ben
 *
 *  This is a port of the stellarisware ustlib.c ulocaltime
 */

#ifndef TIME_H_
#define TIME_H_

//*****************************************************************************
//
//! A structure that contains the broken down date and time.
//
//*****************************************************************************
typedef struct
{
    //
    //! The number of years since 0 AD.
    //
    unsigned short usYear;

    //
    //! The month, where January is 0 and December is 11.
    //
    unsigned char ucMon;

    //
    //! The day of the month.
    //
    unsigned char ucMday;

    //
    //! The day of the week, where Sunday is 0 and Saturday is 6.
    //
    unsigned char ucWday;

    //
    //! The number of hours.
    //
    unsigned char ucHour;

    //
    //! The number of minutes.
    //
    unsigned char ucMin;

    //
    //! The number of seconds.
    //
    unsigned char ucSec;
}
tTime;

void ulocaltime(unsigned long ulTime, tTime *psTime);

#endif /* TIME_H_ */

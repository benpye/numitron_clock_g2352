/*
 * display.h
 *
 *  Created on: 3 Jan 2013
 *      Author: Ben
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

//Define our pins
#define DATA BIT0 // DS -> 1.0
#define CLOCK BIT4 // SH_CP -> 1.4
#define LATCH BIT5 // ST_CP -> 1.5
#define ENABLE BIT7 // OE -> 1.7

void displayDrawDigit( unsigned char num );
void displayDrawNumberDecimal( unsigned int number );

inline void displayEnable()
{
	P1OUT &= ~ENABLE;
}

inline void displayDisable()
{
	P1OUT |= ENABLE;
}

inline void displayOff()
{
	P1OUT &= ~ENABLE;
	P1OUT &= ~LATCH;
	P1OUT &= ~DATA;
	P1OUT &= ~CLOCK;
}

// Base shift register functions
void displayPulseClock ( void );
void displayShiftOut ( unsigned char );

void displayLatchLow( void );
void displayLatchHigh( void );

#endif /* DISPLAY_H_ */

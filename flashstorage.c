/*
 * flashstorage.c
 *
 *  Created on: 4 Jan 2013
 *      Author: Ben
 */

#include <msp430.h>
#include "flashstorage.h"

char g_FlashRamTable[FLASH_RAM_BUFFER_SIZE]; // Allow for 16 bytes currently, if more is needed increase this
char *InfoB = (char *) 0x1080;

void flashInitRamOverlay()
{
	unsigned int i;
	for(i = 0; i < FLASH_RAM_BUFFER_SIZE; i++)
	{
		g_FlashRamTable[i] = InfoB[i];
	}
}

void flashCommitData()
{
	unsigned int i;
	FCTL1 = FWKEY + ERASE;				// Set Erase bit
	FCTL3 = FWKEY;						// Clear Lock bit
	*InfoB = 0;							// Dummy write to erase Flash segment

	FCTL1 = FWKEY + WRT;				// Set WRT bit for write operation

	for ( i=0; i < FLASH_RAM_BUFFER_SIZE; i++ )
	{
		InfoB[i] = g_FlashRamTable[i];			// Write value to flash
	}

	FCTL1 = FWKEY;						// Clear WRT bit
	FCTL3 = FWKEY + LOCK;				// Set LOCK bit
}
